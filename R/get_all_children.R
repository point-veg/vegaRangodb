# get_all_children.R
#'
#' @title Creates a list with all child taxa
#'
#' @description Returns a list of `ArangoDocument`'s with all the lower levels of the given taxon `x`.
#'
#' @param x An `ArangoDocument` corresponding to a taxon in `florula.collection`.
#' @param florula.collection An `ArangoCollection` containing a flora list.
#'
#' @details The flora list in `florula.collection` must follow a specific structure. Check function \code{\link{insert_taxa}} for more details.
#'
#' @return A `list` of `ArangoDocument`'s retrieved from `florula.collection`.
#'
#' @author Tiago Monteiro-Henriques. E-mail: \email{tmh.dev@@icloud.com}.
#'
#' @export
#'
get_all_children <- function(x, florula.collection) {
  if (is.null(x)) {
    return(NULL)
  }
  if (!is_lowest_level(x=x, florula.collection=florula.collection)) {
    res <- get_lower_Adocs(x, florula.collection=florula.collection)
    res <- c(res, lapply(res, get_all_children, florula.collection=florula.collection))
    return(unlist(res))
  }
}
