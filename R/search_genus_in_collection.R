# search_genus_in_collection.R
#'
#' @title Search for a genus name in a flora list
#'
#' @description This function searches for a `genus.name` in `florula.collection`.
#'
#' @param genus.name A `character` with a genus name to search in `florula.collection`.
#' @param florula.collection An `ArangoCollection` containing a flora list.
#'
#' @details It returns an error if more than one match is found in `florula.collection`.
#'
#' @return An `ArangoDocument` corresponding to the searched genus in `florula.collection`. It returns `NULL` if nothing is found.
#'
#' @author Tiago Monteiro-Henriques. E-mail: \email{tmh.dev@@icloud.com}.
#'
#' @export
#'
search_genus_in_collection <- function (genus.name, florula.collection) {
  res <- aRangodb::collection_filter(florula.collection, genus=genus.name)
  lr <- length(res)
  if (lr==1) {return(res[[1]])}
  if (lr>1) {stop("It seems the given genus is duplicated in the database! Check database consistency!")}
  return(NULL)
}
