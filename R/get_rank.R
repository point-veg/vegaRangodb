# get_rank.R
#'
#' @title The rank saved in the `ArangoDocument`
#'
#' @description Returns the rank of the Latin name saved in the `ArangoDocument`.
#'
#' @param x An `ArangoDocument` corresponding to a taxon in a flora list.
#'
#' @details The given `ArangoDocument` is expected to come from a flora list where all documents must have one (and only one) of these attributes: "genus", "epithet", "subspecies", "variety", "form". When the name saved in the `ArangoDocument` is a specific epithet (saved in the attribute "epithet") this function return "species" as rank.
#'
#' @return A `character` with the rank of the name.
#'
#' @author Tiago Monteiro-Henriques. E-mail: \email{tmh.dev@@icloud.com}.
#'
#' @export
#'
get_rank <- function(x) {
  if ("ArangoDocument" %in% class(x)) {
    for_rank <- x$getValues()[c("genus", "epithet", "subspecies", "variety", "form")]
    rank <- names(for_rank[!sapply(for_rank, is.null)])
    if (rank=="epithet") {rank <- "species"}
    return(rank)
  } else {stop("Argument x is not of 'ArangoDocument' class.")}
}
