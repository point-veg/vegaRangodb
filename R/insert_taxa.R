# insert_taxa.R
#'
#' @title Inserts a taxon name into a flora list
#'
#' @description Inserts a taxon name (possibly also parent taxa names), and respective authority (authorities), in the flora list, in case that is (are) not present already.
#'
#' @param list A `list` with, at least, the mandatory components: 'genus', 'epithet' and 'epithet_auth'. For infraspecific taxa (subspecies, variety and form) see Details.
#' @param florula.collection An `ArangoCollection` containing a flora list.
#'
#' @details Possible components of the input list:
#' \describe{
#'   \item{genus*}{the Latin name of the genus}
#'   \item{genus_auth}{the name of the author of the genus}
#'   \item{epithet*}{the Latin specific epithet to subordinate to the given genus}
#'   \item{epithet_auth*}{the name of the author of the genus+epithet combination}
#'   \item{subspecies}{the Latin subspecies name to subordinate to the given species}
#'   \item{subspecies_auth}{the name of the author of the genus+epithet+subspecies combination (mandatory if subspecies is given)}
#'   \item{variety}{the Latin variety name to subordinate to the given subspecies (if given), or species (when a subspecies is not given)}
#'   \item{variety_auth}{the name of the author of the genus+epithet+variety combination (mandatory if variety is given)}
#'   \item{form}{the Latin form name to subordinate to the given variety (if given), or subspecies (if given, and when a variety is not given), or species (when both a variety and a subspecies are not given)}
#'   \item{form_auth}{the name of the author of the genus+epithet+form combination (mandatory if form is given)}
#' }
#'*The 'genus', 'epithet' and 'epithet auth' are always mandatory components of the list.
#'Only the genus rank can be inserted in the database without an authority (but this should be avoided or corrected later, for consistency).
#In the case that 'subspecies', 'variety' or 'form' are components in the list, then 'subspecies_auth', 'variety_auth' and 'form_auth' become mandatory.
#'
#' @return The flora list in `florula.collection` is updated (in the server) to include any of the given Latin names in the `list` and respective author.
#'
#' @author Tiago Monteiro-Henriques. E-mail: \email{tmh.dev@@icloud.com}.
#'
#' @export
#'
#'
# this function allows the insertion of genus names without the respective authority, although this is discouraged ######## VER SE ISTO FAZ SENTIDO!!!!!!!! ######## ACHO QUE NÃO!
# Infraspecific taxa should keep rank consistency within each rank.
# SEE FUNCTION infra.consistency()!!!!!!!!!!!!! ##
#VER SE VALE A PENA: import.extra.fields	a character vector of names of fields to import other than mandatory ones. see Details.


#VER SE VALE A PENA#
#Known extra.fields like genus_auth are saved in the respective rank if import.authors=TRUE (default). Unknown fields are saved in the lowest rank given in the list (species or below).

####ATENÇÃO####
#POR AGORA NÃO EVITA INCONGRUÊNCIA NA ESTRUTURA DOS INFRAESPECÍFICOS!!!!!! Pode ser tratada aqui ou testada a sua consistência mais tarde...
#'
insert_taxa <- function(list, florula.collection) {

  #CHECK IF TAXON IS ALREADY IN THE DATABASE
  #IF SO, ASK IF USER WANTS TO UPDATE THE GIVEN extra.fields (e.g. only update the author of a rank)
  if (!is.null(list$subspecies) & is.null(list$subspecies_auth)) {stop("The attribute subspecies_auth is missing!")}
  if (!is.null(list$variety) & is.null(list$variety_auth)) {stop("The attribute variety_auth is missing!")}
  if (!is.null(list$form) & is.null(list$form_auth)) {stop("The attribute form_auth is missing!")}
  inserted <- FALSE

  if (!is.null(list$genus) & !is.null(list$epithet) & !is.null(list$epithet_auth)) { #In this way it is limited the possibility of having genus without subordinate taxa in the database (will happen only if the function is interrupted).
    genus_Adoc <- search_genus_in_collection(list$genus, florula.collection = florula.collection)
    if (is.null(genus_Adoc)) { ##GENUS IS NOT IN THE DATABASE! INSERT IT? CHECK IPNI AND EURO+MED AND IMPORT FROM THERE EVENTUALLY?
      #Se sim
      list.import1 <- list[c("genus", "genus_auth")] #NULLS are not imported to ArangoDB
      genus_Adoc <- aRangodb::document_insert(florula.collection, key=NULL) %>%
        document_set_LIST(list.import1) %>%
        aRangodb::collection_update()
      message("A new genus was inserted in the database.")
      inserted <- TRUE
      #Se não adicionar à base de dados interrompe o processo!
    }
    epithet_Adoc <- search_species_in_collection(list$genus, list$epithet, florula.collection = florula.collection)
    if (is.null(epithet_Adoc)) { ##SPECIES IS NOT IN THE DATABASE! INSERT IT? CHECK IPNI AND EURO+MED AND IMPORT FROM THERE EVENTUALLY?
      #Se sim
      list.import2 <- c(genus_key=genus_Adoc$getKey(), list[c("epithet", "epithet_auth")])
      epithet_Adoc <- aRangodb::document_insert(florula.collection, key=NULL) %>%
        document_set_LIST(list.import2) %>%
        aRangodb::collection_update()
      message("A new species (epithet) was inserted in the database.")
      inserted <- TRUE
      #Se não adicionar à base de dados interrompe o processo!
    }

  } else {stop("Attributes genus, epithet and epithet_auth must not be NULL!")}

  if (!is.null(list$subspecies)) {
    search_list3 <- search_infraspecific(list$subspecies, epithet_Adoc, florula.collection, attributes.to.search=c("subspecies"))
    if (length(search_list3)==1) {subspecies_Adoc <- search_list3[[1]]}
    if (length(search_list3)>1) {stop("It seems the given subspecies is duplicated within the same species in the database! Check database consistency!")}
    if (length(search_list3)==0) { ##SUBPECIES IS NOT IN THE DATABASE! INSERT IT? CHECK IPNI AND EURO+MED AND IMPORT FROM THERE EVENTUALLY?
      #se sim
      list.import3 <- c(species_key=epithet_Adoc$getKey(), list[c("subspecies", "subspecies_auth")])
      subspecies_Adoc <- aRangodb::document_insert(florula.collection, key=NULL) %>%
        document_set_LIST(list.import3) %>%
        aRangodb::collection_update()
      message("A new subspecies was inserted in the database.")
      inserted <- TRUE
      #Se não adicionar à base de dados interrompe o processo!
    }
  }

  if (!is.null(list$variety)) {
    search_list4 <- search_infraspecific(list$variety, epithet_Adoc, florula.collection, attributes.to.search=c("variety"))
    if (length(search_list4)==1) {variety_Adoc <- search_list4[[1]]}
    if (length(search_list4)>1) {stop("It seems the given variety is duplicated within the same species in the database! Check database consistency!")}
    if (length(search_list4)==0) { ##VARIETY IS NOT IN THE DATABASE! INSERT IT? CHECK IPNI AND EURO+MED AND IMPORT FROM THERE EVENTUALLY?
      #se sim
      if (!is.null(list$subspecies) & !is.null(list$subspecies_auth)) {
        list.import4 <- c(subspecies_key=subspecies_Adoc$getKey(), list[c("variety", "variety_auth")])
        variety_Adoc <- aRangodb::document_insert(florula.collection, key=NULL) %>%
          document_set_LIST(list.import4) %>%
          aRangodb::collection_update()
        message("A new variety was inserted in the database.")
        inserted <- TRUE
      } else {
        list.import4 <- c(species_key=epithet_Adoc$getKey(), list[c("variety", "variety_auth")])
        variety_Adoc <- aRangodb::document_insert(florula.collection, key=NULL) %>%
          document_set_LIST(list.import4) %>%
          aRangodb::collection_update()
        message("A new variety was inserted in the database.")
        inserted <- TRUE
      }
      #Se não adicionar à base de dados interrompe o processo!
    }
  }

  if (!is.null(list$form)) {
    search_list5 <- search_infraspecific(list$form, epithet_Adoc, florula.collection, attributes.to.search=c("form"))
    if (length(search_list5)==1) {return(invisible())}
    if (length(search_list5)>1) {stop("It seems the given form is duplicated within the same species in the database! Check database consistency!")}
    if (length(search_list5)==0) {
      ##FORM IS NOT IN THE DATABASE! INSERT IT? CHECK IPNI AND EURO+MED AND IMPORT FROM THERE EVENTUALLY?
      #se sim
      if (!is.null(list$variety)) {
        list.import5 <- c(variety_key=variety_Adoc$getKey(), list[c("form", "form_auth")])
        aRangodb::document_insert(florula.collection, key=NULL) %>%
          document_set_LIST(list.import5) %>%
          aRangodb::collection_update()
        message("A new form was inserted in the database.")
        inserted <- TRUE
      } else {
        if (!is.null(list$subspecies)) {
          list.import5 <- c(subspecies_key=subspecies_Adoc$getKey(), list[c("form", "form_auth")])
          aRangodb::document_insert(florula.collection, key=NULL) %>%
            document_set_LIST(list.import5) %>%
            aRangodb::collection_update()
          message("A new form was inserted in the database.")
          inserted <- TRUE
        } else {
          list.import5 <- c(species_key=epithet_Adoc$getKey(), list[c("form", "form_auth")])
          aRangodb::document_insert(florula.collection, key=NULL) %>%
            document_set_LIST(list.import5) %>%
            aRangodb::collection_update()
          message("A new form was inserted in the database.")
          inserted <- TRUE
        }
      }
      #Se não adicionar à base de dados interrompe o processo!
    }
  }
  if (!inserted) {message("No taxon was inserted in the database.")}
  return(invisible())
}
