# show_lowest_taxa.R
#'
#' @title Print all lowest children names
#'
#' @description  This function prints the extended canonical names of all the children of the given taxon `x` under the condition that they correspond to a lowest level in `florula.collection`. See function \code{\link{is_lowest_level}}.
#'
#' @param x An `ArangoDocument` corresponding to a taxon in a flora list.
#' @param florula.collection An `ArangoCollection` containing a flora list.
#'
#' @details The given `ArangoDocument` is expected to come from a flora list where all documents must have one (and only one) of these attributes: 'genus', 'epithet', 'subspecies', 'variety', 'form'.
#'
#' @return Prints the found extended canonical names, if any.
#'
#' @author Tiago Monteiro-Henriques. E-mail: \email{tmh.dev@@icloud.com}.
#'
#' @export
#'
show_lowest_taxa <- function (x, florula.collection) {
  list_children <- get_all_children(x, florula.collection)
  if (is.null(list_children)) {NULL} else {
    list_children_lowlev <- list_children[sapply(list_children, is_lowest_level, florula.collection)]
    names_lowlev <- sapply(list_children_lowlev, get_extended_canonical_name, florula.collection)
    print(sort(as.character(names_lowlev)))
  }
}

