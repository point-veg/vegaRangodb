# search_infraspecific.R
#'
#' @title Search an infraspecific name in a flora list (given a species `ArangoDocument`)
#'
#' @description This function searches for a given `infraspecific.name` in `florula.collection`, constraining the search to a given species and allowing the user to select which attributes to search in.
#'
#' @param infraspecific.name A `character` with an infraspecific Latin name to search within a given species.
#' @param x.species An `ArangoDocument` corresponding to the species rank in `florula.collection`, to which the search should be constrained.
#' @param attributes.to.search A `character` vector with the attributes to search in (any of 'subspecies', 'variety' and 'form').
#' @param florula.collection An `ArangoCollection` containing a flora list.
#'
#' @details The given `infraspecific.name` is searched separately in each of the given attributes (one by one), for all `ArangoDocument`s that (directly or indirectly) point at the given species (`x.species`) and the results are collected in a list.
#'
#' @return A `list` with the `ArangoDocument`s in the `florula.collection` from which infraspecific Latin names match the given `infraspecific.name` in the `attributes.to.search`. The function returns `NULL` if no match is found.
#'
#' @author Tiago Monteiro-Henriques. E-mail: \email{tmh.dev@@icloud.com}.
#'
#' @export
#'
search_infraspecific <- function (infraspecific.name, x.species, florula.collection, attributes.to.search=c("subspecies", "variety", "form")) {
  INFRA <- get_all_children(x.species, florula.collection = florula.collection)
  return(INFRA[sapply(INFRA, is_name_in_ADoc, name = infraspecific.name, attributes.to.search = attributes.to.search)])
}
