# search_species_in_collection.R
#'
#' @title Search for a species name in a flora list
#'
#' @description This function searches for a species ('genus' + 'epithet') in `florula.collection`.
#'
#' @param genus.name A `character` with a genus name to search in `florula.collection`.
#' @param epithet.name A `character` with a specific epithet to search in `florula.collection` whithin the genus given in `genus.name`.
#' @param florula.collection An `ArangoCollection` containing a flora list.
#'
#' @details It returns an error if more than one match is found in `florula.collection`.
#'
#' @return An `ArangoDocument` corresponding to the searched species in `florula.collection`. It returns `NULL` if nothing is found.
#'
#' @author Tiago Monteiro-Henriques. E-mail: \email{tmh.dev@@icloud.com}.
#'
#' @export
#'
search_species_in_collection <- function (genus.name, epithet.name, florula.collection) {
  GENUS <- search_genus_in_collection(genus.name = genus.name, florula.collection = florula.collection)
  SPECIES <- aRangodb::collection_filter(florula.collection, genus_key=GENUS$getKey(), epithet=epithet.name)
  ls <- length(SPECIES)
  if (ls==1) {return(SPECIES[[1]])}
  if (ls>1) {stop("It seems the given epithet is duplicated within the same genus in the database! Check database consistency!")}
  return(NULL)
}
