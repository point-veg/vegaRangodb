# is_lowest_level.R
#'
#' @title Is lowest level in flora list?
#'
#' @description Returns `TRUE` if there are no lower ranks in `florula.collection` referring to the given taxon `x`.
#'
#' @param x An `ArangoDocument` corresponding to a taxon in `florula.collection`.
#' @param florula.collection An `ArangoCollection` containing a flora list.
#'
#' @details The flora list in `florula.collection` must follow a specific structure. Check function \code{\link{insert_taxa}} for more details.
#'
#' @return `logical`.
#'
#' @author Tiago Monteiro-Henriques. E-mail: \email{tmh.dev@@icloud.com}.
#'
#' @export
#'
is_lowest_level <- function(x, florula.collection) {
  #auxiliary objects for this function. is there a way of making them allways availabe instead of creating them each time function is called?
  GESV <- c("genus", "epithet", "subspecies", "variety")
  GESV_ordered <- factor(GESV, levels=rev(GESVF), ordered=TRUE)
  GESV_key <- c("genus_key", "species_key", "subspecies_key", "variety_key")
  GESVF <- c("genus", "epithet", "subspecies", "variety", "form")

  if ("ArangoDocument" %in% class(x)) {
    for_name <- x$getValues()[GESVF]
    name <- names(for_name[!sapply(for_name, is.null)])
    if (name=="form") {return(TRUE)}
    key <- x$getKey()
    ADoclist <- list()
    for (i in GESV_key[GESV_ordered <= name]) {
      names(key) <- i
      list_args <- c(florula.collection, key)
      ADoclist_temp <- do.call(aRangodb::collection_filter, list_args)
      ADoclist <- c(ADoclist, ADoclist_temp)
    }
    if (length(ADoclist)>0) {return(FALSE)} else {return(TRUE)}
  }
}
