# get_name.R
#'
#' @title The name saved in the `ArangoDocument`
#'
#' @description Returns the Latin name saved in the given `ArangoDocument`.
#'
#' @param x An `ArangoDocument` corresponding to a taxon in a flora list.
#'
#' @details The given `ArangoDocument` is expected to come from a flora list where all documents must have one (and only one) of these attributes: "genus", "epithet", "subspecies", "variety", "form".
#'
#' @return A `character` with the Latin name.
#'
#' @author Tiago Monteiro-Henriques. E-mail: \email{tmh.dev@@icloud.com}.
#'
#' @export
#'
get_name <- function(x) {
  if ("ArangoDocument" %in% class(x)) {
    name <- x$getValues()[c("genus", "epithet", "subspecies", "variety", "form")]
    return(unlist(name))
  } else {stop("Argument x is not of 'ArangoDocument' class.")}
}
