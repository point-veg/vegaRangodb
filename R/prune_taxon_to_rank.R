# prune_taxon_to_rank.R
#'
#' @title Prune taxon to a user-defined rank
#'
#' @description To a user-defined `rank` level, this function returns the `ArangoDocument` with the lowest rank existing in `florula.collection` selecting from all the parent taxa of a given taxon and the given taxon itself.
#'
#' @param x An `ArangoDocument` corresponding to a taxon in a flora list.
#' @param rank A `character` with the rank level to prune to.
#' @param florula.collection An `ArangoCollection` containing a flora list.
#'
#' @details The given `ArangoDocument` is expected to come from a flora list where all documents must have one (and only one) of these attributes: "genus", "epithet", "subspecies", "variety", "form".
#'
#' @return An `ArangoDocument` corresponding to the user-defined rank (if present) or the lowest rank existing in `florula.collection`.
#'
#' @author Tiago Monteiro-Henriques. E-mail: \email{tmh.dev@@icloud.com}.
#'
#' @export
#'
prune_taxon_to_rank <- function(x, rank, florula.collection) {
  actual.rank <- get_rank(x)
  if (actual.rank=="genus") {return(x)}
  if (rank=="genus") {return(get_parent_genus(x, florula.collection))}
  if (rank=="species") {return(get_parent_species(x, florula.collection))}
  y <- append_all_parents(x, florula.collection)
  existing.ranks <- sapply(y, function (z) {get_rank(z)})
  if (any(SVF %in% existing.ranks)) {
    rank.res.temp <- SVF_ordered >= rank & SVF %in% existing.ranks
    if (any(rank.res.temp)) {
      rank.res <- min(SVF_ordered[rank.res.temp])
      return(y[[which(existing.ranks %in% rank.res)]])
    } else {return(get_parent_species(x, florula.collection))}
  } else {return(get_parent_species(x, florula.collection))}
}
