
<!-- README.md is generated from README.Rmd. Please edit that file -->

# vegaRangodb

<!-- badges: start -->
<!-- badges: end -->

A toolset (under development!) to manage vegetation data in ArangoDB,
covering several issues common in vegetation science, namely, storage
and maintenance of flora lists, storage and management of relevés,
relevé georreferencing, correction of taxon names used in relevés
keeping record the associated context, semi-automatic and contextual
correction of taxon names, taxonomic harmonization of relevés to flora
lists, preparing of phytosociological tables for data analysis following
certain flora lists.

## Acknowledgments

T.M.H. was funded by the European Social Fund (POCH and NORTE 2020) and
by National Funds (MCTES), through a FCT – Fundação para a Ciência a
Tecnologia (Portuguese Foundation for Science and Technology)
postdoctoral fellowship (SFRH/BPD/115057/2016), as well as by National
Funds, through the same foundation, under the project UIDB/04033/2020
(CITAB - Centre for the Research and Technology of Agro-Environmental
and Biological Sciences).

## Installation

You can install the package from GitLab.

``` r
devtools::install_git("https://gitlab.com/point-veg/vegaRangodb")
```
